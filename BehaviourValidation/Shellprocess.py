""" shell_process.py
this folder allows to lunch all shell processes
author = Laetitia GIABRT
"""

import os.path
import shutil
import subprocess


def rm_dir(path):
    if (os.path.exists(path) == True):
        subprocess.run(["rm", "-rf", path])


def rm_dir_py(dir_path):
    if (os.path.exists(dir_path) == True):
        try:
            shutil.rmtree(dir_path)
        except OSError as e:
            print("Error: %s : %s" % (dir_path, e.strerror))


def file_copy(source_path, targeted_path):
    subprocess.run(["cp", source_path, targeted_path])


def run_Totembionet(option, path):
    #print(path)
    p = subprocess.Popen(['totembionet', option, path], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    std_out, std_err = p.communicate()
    return std_out, std_err

def run_totembionet_from(pathcsv, pathsmb):
    p = subprocess.popen(['totembionet', pathsmb, "-from", pathcsv, '-csv' , '-verbose' ,'3'], stdout=subprocess.pipe, stderr=subprocess.pipe)
    std_out, std_err = p.communicate()
    #print(std_err)
    return std_out, std_err

def results_filling_by_var(path_results):
    # print(type(path_results))
    subprocess.run(["mkdir", "-p", path_results])


def results_filling(out_file):
    """ Args:

	    Param1 : input CSV file

	      Return:

	    create copy of results and smb file il result directory depending on Envars values

	 """
    subprocess.run(["mkdir", "-p", path_test])
    targeted_path = "./results/"
    file_copy("/" + out_file + ".out",
              targeted_path)  # aps sur de mettre le. out focntion de ce qui est dans out-file perso je pense spas qu'il soit utile
